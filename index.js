// Bài 1 : Quản lý tuyển sinh

// điểm ưu tiên khu vực

function diemKhuVuc(khuVuc) {
  var diemKhuVuc;

  if (khuVuc == 'A') {
    diemKhuVuc = 2;
  } else if (khuVuc == 'B') {
    diemKhuVuc = 1;
  } else if (khuVuc == 'C') {
    diemKhuVuc = 0.5;
  } else {
    diemKhuVuc = 0;
  }
  return diemKhuVuc;
}

// điểm ưu tiên đối tượng

function diemDoiTuong(doiTuong) {
  var diemDoiTuong;

  if (doiTuong == '1') {
    diemDoiTuong = 2.5;
  } else if (doiTuong == '2') {
    diemDoiTuong = 1.5;
  } else if (doiTuong == '3') {
    diemDoiTuong = 1;
  } else {
    diemDoiTuong = 0;
  }
  return diemDoiTuong;
}

// kết quả
function chamDiem() {
  var khuVuc = document.getElementById('khu_vuc').value;
  var doiTuong = document.getElementById('doi_tuong').value;
  var diemChuan = document.getElementById('diem_chuan').value * 1;
  var diemMon1 = document.getElementById('diem_mon_1').value * 1;
  var diemMon2 = document.getElementById('diem_mon_2').value * 1;
  var diemMon3 = document.getElementById('diem_mon_3').value * 1;
  var khuVuc = document.getElementById('khu_vuc').value;
  var diemTong3Mon = diemMon1 + diemMon2 + diemMon3;
  var diemUuTien = diemKhuVuc(khuVuc) + diemDoiTuong(doiTuong);
  var diemTongKet = diemTong3Mon + diemUuTien;
  var result = document.getElementById('result');

  if (
    diemMon1 < 0 ||
    diemMon2 < 0 ||
    diemMon3 < 0 ||
    diemChuan < 0 ||
    diemMon1 > 10 ||
    diemMon2 > 10 ||
    diemMon3 > 10 ||
    diemChuan > 30
  ) {
    return (result.innerHTML = `Giá trị không hợp lệ do số điểm nhập không được lớn hơn 10 và nhỏ hơn 0, điểm chuấn phải lớn hơn 0 và nhỏ hơn 30`);
  }
  if (
    diemMon1 !== 0 &&
    diemMon2 !== 0 &&
    diemMon3 !== 0 &&
    diemTongKet >= diemChuan
  ) {
    result.innerHTML = `Bạn đã đậu, Tổng điểm đạt được là: ${diemTongKet}`;
  } else {
    result.innerHTML = `Bạn đã rớt, Tổng điểm đạt được là: ${diemTongKet}`;
  }
}

// Bài 2 : Tính tiền điện
// giá tiền theo kw sử dụng

function tinhTienDien() {
  var hoTen = document.getElementById('ho_ten').value;
  var soKw = document.getElementById('so_kw').value * 1;
  var tinhTien = document.getElementById('result2');

  var giaTien;
  if (soKw < 0) {
    return (tinhTien.innerHTML = `Số kư không được nhỏ hơn 0`);
  }
  if (soKw >= 0 && soKw <= 50) {
    giaTien = 500 * soKw;
  } else if (soKw > 50 && soKw <= 100) {
    giaTien = 500 * 50 + 650 * (soKw - 50);
  } else if (soKw > 100 && soKw <= 200) {
    giaTien = 500 * 50 + 650 * 50 + (soKw - 100) * 850;
  } else if (soKw > 200 && soKw <= 350) {
    giaTien = 500 * 50 + 650 * 50 + 850 * 100 + (soKw - 200) * 1100;
  } else {
    giaTien =
      500 * 50 + 650 * 50 + 850 * 100 + 150 * 1100 + (soKw - 350) * 1300;
  }
  tinhTien.innerHTML = `Họ tên : ${hoTen} , Tiền điện phải đóng là: ${new Intl.NumberFormat(
    'de-DE',
    {
      style: 'currency',
      currency: 'VND',
    }
  ).format(giaTien)}`;
}
